import { useState } from "react";

const useForm = (initialFieldValues, validate, setCurrentId ) => {
    const [values, setValues] = useState(initialFieldValues)
    const [errors, setErrors] = useState({})
    const [submitorupdate, setSubmitOrUpdate] = useState(true);

   


    const handleInputChange = e => {
        const { name, value } = e.target
        const fieldValue = { [name]: value }
        setValues({
            ...values,
            ...fieldValue,
            
        })
        setSubmitOrUpdate(true)
        validate(fieldValue)
    }

    const resetForm = () => {
        setValues({
            ...initialFieldValues, age:18, mobile:"07....."
        })
        setErrors({})
        setCurrentId(0)
        setSubmitOrUpdate(true)
    }

    return {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm,
        setSubmitOrUpdate,
        submitorupdate
    };
}

export default useForm;