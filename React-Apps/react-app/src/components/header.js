import React, { useState } from "react";
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import '../styles/header.css'
import { TextField } from "@material-ui/core";
import { store } from "../actions/store";
import { Provider } from "react-redux";
import DCandidates from '../components/DCandidates';
import { Container } from "@material-ui/core";
import { ToastProvider } from "react-toast-notifications";
import Footer from '../components/footer'
import About from '../components/about'
import Help from "@material-ui/icons/Help";
import { SettingsInputComponent } from "@material-ui/icons";



const Header = () => {

  const [open, setOpen] = React.useState(false);
  const [openHelp, setOpenHelp] = React.useState(false);
  const [loggedIn, setLogin] = React.useState(false);
  const [email, setemail] = useState("")
  const [password, setpassword] = useState("")
  
  
  
  const handleClickOpen = () => {
    if (loggedIn)
      setLogin(false)

 
    else
{
  setemail("")
  setpassword("")
  setOpen(true);


  

}
  };


  const handleClickHelpOpen = () => {
    setOpenHelp(true)
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleHelpClose = () => {
    setOpenHelp(false);
  };

  const autoLogin = () => {
    setOpen(false);
    setLogin(true)
  };


  const Login = () => {
    setOpen(false)
    
    if (email.toUpperCase().indexOf("DROP")>=0 || email.toUpperCase().indexOf("INSERT")>=0 || email.toUpperCase().indexOf("SELECT")>=0)
      alert('Are you trying to hack us?')
    else
    {
      if (email==="test" && password=="test")
      setLogin(true);
    else
      setLogin(false);
    }


  }


  const handleEmailInputChange = e => {
    setemail(e.target.value)
  }

  const handlePasswordInputChange = e => {
    setpassword(e.target.value)
  }


    return (

      <Provider store={store}>
      <ToastProvider autoDismiss={true}>
        <Container maxWidth="lg">

        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="#home">React Dreams</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
            <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Nav>
            <Nav.Link href="#deets">More deets</Nav.Link>
            <Nav.Link eventKey={2} href="#memes">
              Dank memes
            </Nav.Link>

          

 
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Login</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email Address"
            type="text"
            value={email}
            onChange={handleEmailInputChange}
            fullWidth
          />
          
          <TextField
           
            margin="dense"
            id="password"
            label="Password"
            type="password"
            value={password}
            onChange={handlePasswordInputChange}
            fullWidth
          />


        </DialogContent>
        <DialogActions>
        <Button onClick={autoLogin} color="primary">
            Auto-Login
          </Button>
          <Button onClick={Login} color="primary">
            Login
          </Button>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>


      <Dialog open={openHelp} onClose={handleHelpClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Help</DialogTitle>
        <DialogContent>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleHelpClose} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>


        <Button className="btn" onClick={handleClickHelpOpen}>Help</Button>
        <Button className="btn" type="submit"  onClick={handleClickOpen}>{loggedIn ? "Logout" : "Login"}</Button>
          </Nav>
        </Navbar.Collapse>
      </Navbar>


{/* comment */}
      {loggedIn ? <DCandidates /> : <About />}




          <Footer />

        </Container>
      </ToastProvider>
    </Provider>

    )
}

export default Header